package com.example.icehouseonboard.di

import com.example.icehouseonboard.platform.database.DatabaseAccessor
import com.example.icehouseonboard.platform.database.DatabaseAccessorImpl
import com.example.icehouseonboard.platform.preference.CachedTokenManager
import com.example.icehouseonboard.platform.preference.CachedTokenManagerImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class UtilsModule {
    @Binds
    @Singleton
    abstract fun bindDatabaseAccessorWith(dbAccessor: DatabaseAccessorImpl): DatabaseAccessor

    @Binds
    @Singleton
    abstract fun bindCachedTokenManagerWith(
        cacheTokenManagerImpl: CachedTokenManagerImpl
    ): CachedTokenManager
}
