package com.example.icehouseonboard.di

import com.example.icehouseonboard.platform.api.ContentApi
import com.example.icehouseonboard.platform.api.CredentialApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    @Provides
    @Singleton
    fun provideCredentialApi(retrofit: Retrofit): CredentialApi {
        return retrofit.create(CredentialApi::class.java)
    }

    @Provides
    @Singleton
    fun provideContentApi(retrofit: Retrofit): ContentApi {
        return retrofit.create(ContentApi::class.java)
    }
}
