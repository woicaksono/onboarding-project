package com.example.icehouseonboard.di

import com.example.icehouseonboard.platform.gateway.ContentGateway
import com.example.icehouseonboard.platform.gateway.ContentGatewayImpl
import com.example.icehouseonboard.platform.gateway.CredentialGateway
import com.example.icehouseonboard.platform.gateway.CredentialGatewayImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class GatewayModule {
    @Binds
    @Singleton
    abstract fun bindCredentialGatewayWith(
        credentialGatewayImpl: CredentialGatewayImpl
    ): CredentialGateway

    @Binds
    @Singleton
    abstract fun bindContentGatewayWith(
        contentGatewayImpl: ContentGatewayImpl
    ): ContentGateway
}