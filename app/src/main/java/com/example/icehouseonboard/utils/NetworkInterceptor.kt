package com.example.icehouseonboard.utils

import com.example.icehouseonboard.domain.model.AuthFailureException
import com.example.icehouseonboard.domain.model.FailedReason
import com.example.icehouseonboard.platform.api.REQUIRED_AUTH
import com.example.icehouseonboard.platform.api.REQUIRED_AUTH_KEY
import com.example.icehouseonboard.platform.api.response.AuthFailedResponse
import com.example.icehouseonboard.platform.preference.CachedTokenManager
import kotlinx.serialization.json.Json
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody
import java.io.IOException

class NetworkInterceptor(private val cachedTokenManager: CachedTokenManager) {
    fun getInterceptor() = okhttp3.Interceptor { chain ->
        chain.request()
            .let { request ->
                if (request.requireAuthentication()) request.addHeaderToken()
                else request
            }.let {
                val response = chain.proceed(it)

                if (!response.isSuccessful) {
                    handleUnsuccessful(response)
                }

                response
            }
    }

    private fun Request.requireAuthentication() = !headers(REQUIRED_AUTH_KEY).isNullOrEmpty()

    private fun Request.addHeaderToken(): Request {
        val token = cachedTokenManager.getToken()

        return newBuilder().removeHeader(REQUIRED_AUTH)
            .header("Authorization", "$token")
            .build()
    }

    private fun handleUnsuccessful(response: Response) {
        when (response.code) {
            400, 401, 422 -> {
                throwAuthFailureWithMessage(response.body)
            }
            else -> throw IOException("Unexpected response $response")
        }

    }

    private fun throwAuthFailureWithMessage(body: ResponseBody?) {
        val failedResponse = body?.string()?.let {
            Json.decodeFromString(AuthFailedResponse.serializer(), it)
        }

        throw (AuthFailureException(FailedReason(failedResponse?.message ?: "")))
    }
}
