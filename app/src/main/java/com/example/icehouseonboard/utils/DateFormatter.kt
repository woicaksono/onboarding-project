package com.example.icehouseonboard.utils

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateFormatter {
    fun stringToDate(dateInString: String): Date {
        return try {
            getFormatter().parse(dateInString)
        } catch (e: ParseException) {
            throw e
        }

    }

    fun beautifyDateAsString(date: Date): String {
        return try {
            getFormatter().format(date)
        } catch (e: ParseException) {
            throw e
        }
    }

    private fun getFormatter(): DateFormat {
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
    }
}
