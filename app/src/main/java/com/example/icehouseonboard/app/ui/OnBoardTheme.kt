package com.example.icehouseonboard.app.ui

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable

@Composable
fun OnBoardTheme(content: @Composable () -> Unit) {
    MaterialTheme(
        typography = typography,
        content = content,
    )
}
