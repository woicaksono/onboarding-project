package com.example.icehouseonboard.app.login

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.icehouseonboard.R
import com.example.icehouseonboard.app.common.PasswordVisibility
import com.example.icehouseonboard.app.common.PasswordVisibility.Obscured
import com.example.icehouseonboard.app.ui.*

const val loginRoute = "login"

@Composable
fun LoginScreen(viewModel: LoginViewModel, onSuccessLogin: () -> Unit) {
    val loginEvent by viewModel.loginEvent.observeAsState(initial = LoginEvent.Idle)
    val userNameState by viewModel.userNameState.observeAsState(initial = "")
    val passwordState by viewModel.passwordState.observeAsState(initial = "")
    val passwordVisibility by viewModel.isPasswordVisible.observeAsState(initial = Obscured)

    if (loginEvent is LoginEvent.Success) onSuccessLogin()

    OnBoardTheme {
        Box(contentAlignment = Alignment.TopCenter) {
            LoginTitle()

            LoginForm(
                loginEvent = loginEvent,
                userNameState = userNameState,
                onUserNameChange = viewModel::onUsernameChange,
                passwordState = passwordState,
                onPasswordChange = viewModel::onPasswordChange,
                passwordVisibility = passwordVisibility,
                onPasswordVisibilityChange = viewModel::onPasswordVisibilityChange,
                onLoginButtonClicked = viewModel::login
            )
        }
    }
}

@Composable
fun LoginTitle() {
    Text(
        text = stringResource(id = R.string.logo_title),
        style = typography.h1,
        modifier = Modifier.padding(top = 62.dp)
    )
}

@Composable
fun LoginForm(
    loginEvent: LoginEvent,
    userNameState: String,
    passwordState: String,
    passwordVisibility: PasswordVisibility,
    onUserNameChange: (String) -> Unit,
    onPasswordChange: (String) -> Unit,
    onPasswordVisibilityChange: () -> Unit,
    onLoginButtonClicked: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .padding(start = 20.dp, end = 20.dp)
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        UsernameField(userNameState, onUserNameChange)

        Spacer(modifier = Modifier.size(32.dp))

        PasswordField(
            password = passwordState,
            onPasswordChange = onPasswordChange,
            passwordVisibility = passwordVisibility,
            onPasswordVisibilityChange = onPasswordVisibilityChange,
        )

        if (loginEvent is LoginEvent.Failed) {
            ErrorTextMessage(loginEvent.message)
        }

        Spacer(modifier = Modifier.size(53.dp))

        LoginButton(loginEvent, onLoginButtonClicked)
    }
}

@Composable
fun UsernameField(userName: String, onUserNameChange: (String) -> Unit) {
    OnBoardInputText(
        label = stringResource(id = R.string.email_label),
        value = userName,
        onValueChange = onUserNameChange,
        keyboardType = KeyboardType.Email,
    )
}

@Composable
fun PasswordField(
    password: String,
    onPasswordChange: (String) -> Unit,
    passwordVisibility: PasswordVisibility,
    onPasswordVisibilityChange: () -> Unit,
) {
    OnBoardPasswordInputText(
        value = password,
        onValueChange = onPasswordChange,
        passwordVisibility = passwordVisibility,
        onPasswordIconClicked = onPasswordVisibilityChange,
    )
}

@Composable
fun ErrorTextMessage(errorMessage: String) {
    Text(
        text = errorMessage,
        style = typography.body2,
        color = Color.Red,
        modifier = Modifier
            .fillMaxWidth()
            .paddingFromBaseline(top = 14.dp),
        textAlign = TextAlign.Start,
    )
}

@Composable
fun LoginButton(loginEvent: LoginEvent, onLoginButtonClicked: () -> Unit) {
    Button(
        enabled = loginEvent != LoginEvent.Loading,
        modifier = Modifier
            .fillMaxWidth()
            .height(48.dp),
        onClick = onLoginButtonClicked,
        colors = ButtonDefaults.buttonColors(
            backgroundColor = buttonColor,
        ),
    ) {
        Text(stringResource(id = R.string.login_label), color = Color.White)
    }
}
