package com.example.icehouseonboard.app.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import com.example.icehouseonboard.R
import com.example.icehouseonboard.app.common.PasswordVisibility

@Composable
fun OnBoardInputText(
    label: String,
    value: String,
    passwordVisibility: PasswordVisibility = PasswordVisibility.Visible,
    keyboardType: KeyboardType = KeyboardType.Text,
    onValueChange: (String) -> Unit,
    onTrailingIconClickedIfAny: (() -> Unit) = {},
) {
    fun getVisualTransformationByPasswordVisibility(): VisualTransformation {
        return if (passwordVisibility == PasswordVisibility.Obscured) PasswordVisualTransformation()
        else VisualTransformation.None
    }

    TextField(
        modifier = Modifier.fillMaxWidth(),
        value = value,
        onValueChange = onValueChange,
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.White,
            textColor = textColor,
            placeholderColor = labelColor,
            focusedLabelColor = labelColor,
            unfocusedLabelColor = labelColor,
            focusedIndicatorColor = labelColor,
            unfocusedIndicatorColor = labelColor,
        ),
        keyboardOptions = KeyboardOptions(keyboardType = keyboardType),
        label = { Text(text = label) },
        visualTransformation = getVisualTransformationByPasswordVisibility(),
        trailingIcon = {
            if (keyboardType == KeyboardType.Password) PasswordIconButton(
                passwordVisibility,
                onTrailingIconClickedIfAny
            )
        }
    )
}

@Composable
fun OnBoardPasswordInputText(
    value: String,
    passwordVisibility: PasswordVisibility,
    onValueChange: (String) -> Unit,
    onPasswordIconClicked: () -> Unit
) {
    OnBoardInputText(
        keyboardType = KeyboardType.Password,
        label = stringResource(id = R.string.password_label),
        value = value,
        passwordVisibility = passwordVisibility,
        onValueChange = onValueChange,
        onTrailingIconClickedIfAny = onPasswordIconClicked,
    )
}

@Composable
fun PasswordIconButton(passwordVisibility: PasswordVisibility, onTrailingIconClicked: () -> Unit) {
    IconButton(onClick = onTrailingIconClicked) {
        passwordVisibility
            .let {
                if (it == PasswordVisibility.Visible)
                    Icons.Filled.Visibility
                else
                    Icons.Filled.VisibilityOff
            }
            .let {
                Icon(imageVector = it, contentDescription = passwordVisibility.toString())
            }
    }
}