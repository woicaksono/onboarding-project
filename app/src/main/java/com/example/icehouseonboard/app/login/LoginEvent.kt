package com.example.icehouseonboard.app.login

sealed class LoginEvent {
    object Idle : LoginEvent()
    object Loading : LoginEvent()
    object Success : LoginEvent()
    data class Failed(val message: String) : LoginEvent()
}
