package com.example.icehouseonboard.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class OnBoardApplication : Application()