package com.example.icehouseonboard.app.ui

import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

val LightTheme = lightColors(
    primary = Color.White,
    secondary = Color(0xff282828),
)

val textColor = Color(0xff222222)
val labelColor = Color(0xff888888)
val buttonColor = Color(0xff282828)