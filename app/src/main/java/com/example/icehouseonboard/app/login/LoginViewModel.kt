package com.example.icehouseonboard.app.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.icehouseonboard.app.common.PasswordVisibility
import com.example.icehouseonboard.domain.model.AuthFailureException
import com.example.icehouseonboard.domain.model.Token
import com.example.icehouseonboard.domain.usecase.LoginUseCase
import com.example.icehouseonboard.domain.usecase.LoginUseCase.Params
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val useCase: LoginUseCase) : ViewModel() {

    private val loginEventEmitter = MutableLiveData<LoginEvent>(LoginEvent.Idle)
    private val userNameStateEmitter = MutableLiveData<String>()
    private val passwordStateEmitter = MutableLiveData<String>()
    private val passwordVisibilityEmitter = MutableLiveData(PasswordVisibility.Obscured)

    val loginEvent = loginEventEmitter
    val userNameState = userNameStateEmitter
    val passwordState = passwordStateEmitter
    val isPasswordVisible: LiveData<PasswordVisibility> = passwordVisibilityEmitter

    fun login() {
        val userName = userNameState.value.orEmpty()
        val password = passwordState.value.orEmpty()
        loginEventEmitter.value = LoginEvent.Loading

        useCase(Params(userName, password), viewModelScope) {
            it.fold(::handleLoginState, ::handleFailure)
        }
    }

    fun onUsernameChange(userName: String) {
        userNameStateEmitter.value = userName
    }

    fun onPasswordChange(password: String) {
        passwordStateEmitter.value = password
    }

    fun onPasswordVisibilityChange() {
        if (isPasswordVisible.value == PasswordVisibility.Visible) {
            passwordVisibilityEmitter.value = PasswordVisibility.Obscured
        } else {
            passwordVisibilityEmitter.value = PasswordVisibility.Visible
        }
    }

    private fun handleLoginState(token: Token) {
        loginEventEmitter.value = LoginEvent.Success
    }

    private fun handleFailure(exception: Throwable) {
        when (exception) {
            is AuthFailureException -> {
                loginEventEmitter.value = LoginEvent.Failed(exception.reason.message)
            }
        }
    }
}
