package com.example.icehouseonboard.app.common

enum class PasswordVisibility {
    Visible, Obscured
}
