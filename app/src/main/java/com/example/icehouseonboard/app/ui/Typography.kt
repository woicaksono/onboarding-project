package com.example.icehouseonboard.app.ui

import androidx.compose.material.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.font.FontWeight.Companion.W400
import androidx.compose.ui.unit.sp
import com.example.icehouseonboard.R

val openSansNormal = FontFamily(Font(R.font.opensans_normal))
val vollkorn = FontFamily(Font(R.font.vollkorn))

val typography = Typography(
    h1 = TextStyle(
        fontFamily = vollkorn,
        fontWeight = W400,
        fontSize = 28.sp
    ),
    body1 = TextStyle(
        fontFamily = openSansNormal,
        fontWeight = W400,
        fontSize = 14.sp
    ),
    body2 = TextStyle(
        fontFamily = openSansNormal,
        fontWeight = W400,
        fontSize = 12.sp
    ),
    button = TextStyle(
        fontFamily = openSansNormal,
        fontWeight = FontWeight.W700,
        fontSize = 16.sp
    ),
)
