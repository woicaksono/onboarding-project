package com.example.icehouseonboard.app

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.icehouseonboard.app.home.HomeScreen
import com.example.icehouseonboard.app.home.homeRoute
import com.example.icehouseonboard.app.login.LoginScreen
import com.example.icehouseonboard.app.login.LoginViewModel
import com.example.icehouseonboard.app.login.loginRoute


class AppGraphNavigation(navController: NavController) {
    val navigateToHome: () -> Unit = {
        navController.navigate(homeRoute)
    }
}

@Composable
fun AppGraph() {
    val navController = rememberNavController()
    val action = remember(navController) { AppGraphNavigation(navController) }

    NavHost(navController = navController, startDestination = loginRoute) {
        composable(loginRoute) {
            val viewModel = hiltViewModel<LoginViewModel>()

            LoginScreen(viewModel, action.navigateToHome)
        }
        composable(homeRoute) { HomeScreen() }
    }
}
