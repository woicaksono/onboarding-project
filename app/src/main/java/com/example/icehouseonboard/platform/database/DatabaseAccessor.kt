package com.example.icehouseonboard.platform.database

import com.example.icehouseonboard.platform.database.dao.NewsDao
import com.example.icehouseonboard.platform.database.dao.ProfileDao
import javax.inject.Inject

interface DatabaseAccessor {

    val newsDao: NewsDao
    val profileDao: ProfileDao

    fun close()
    fun clearAllData()
}

class DatabaseAccessorImpl @Inject constructor(
    private val onBoardDatabase: OnBoardDatabase
) : DatabaseAccessor {
    override val newsDao: NewsDao
        get() = onBoardDatabase.newsDao()

    override val profileDao: ProfileDao
        get() = onBoardDatabase.profileDao()

    override fun close() {
        onBoardDatabase.close()
    }

    override fun clearAllData() {
        onBoardDatabase.clearAllTables()
    }
}
