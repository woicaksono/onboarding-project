package com.example.icehouseonboard.platform.api.response

import com.example.icehouseonboard.domain.model.Profile
import kotlinx.serialization.Serializable

@Serializable
data class ProfileResponse(
    val username: String,
    val name: String,
    val bio: String,
    val web: String,
    val picture: String,
) {
    companion object {
        fun ProfileResponse.mapToDomainModel(): Profile {
            return Profile(username, name, bio, web, picture)
        }

    }
}
