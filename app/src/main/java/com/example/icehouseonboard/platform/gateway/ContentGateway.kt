package com.example.icehouseonboard.platform.gateway

import com.example.icehouseonboard.domain.model.News
import com.example.icehouseonboard.domain.model.Profile
import com.example.icehouseonboard.platform.api.ContentApi
import com.example.icehouseonboard.platform.api.response.NewsResponse.Companion.mapToDomainModel
import com.example.icehouseonboard.platform.api.response.ProfileResponse.Companion.mapToDomainModel
import javax.inject.Inject

interface ContentGateway {
    suspend fun getProfile(): Result<Profile>
    suspend fun getNews(): Result<List<News>>
}

class ContentGatewayImpl @Inject constructor(private val api: ContentApi) : ContentGateway {
    override suspend fun getProfile(): Result<Profile> {
        return safelyExecute { api.getProfile().mapToDomainModel() }
    }

    override suspend fun getNews(): Result<List<News>> {
        return safelyExecute { api.getNews().data.map { it.mapToDomainModel() } }
    }
}
