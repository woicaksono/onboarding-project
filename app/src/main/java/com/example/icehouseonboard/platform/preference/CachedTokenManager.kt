package com.example.icehouseonboard.platform.preference

import android.content.SharedPreferences
import com.example.icehouseonboard.domain.model.Token
import com.example.icehouseonboard.utils.DateFormatter
import javax.inject.Inject

interface CachedTokenManager {
    fun getToken(): String?
    fun getExpiredDateToken(): String?
    fun cacheToken(token: Token)
}

class CachedTokenManagerImpl @Inject constructor(
    private val sharedPreference: SharedPreferences
) : CachedTokenManager {
    override fun getToken(): String? {
        return sharedPreference.getString(TOKEN_KEY, null)
    }

    override fun getExpiredDateToken(): String? {
        return sharedPreference.getString(TOKEN_EXPIRED_AT, null)
    }

    override fun cacheToken(token: Token) {
        storeToken("${token.scheme} ${token.key}")
        storeExpiredDateToken(DateFormatter().beautifyDateAsString(token.expiresAt))
    }

    // region private
    private fun storeToken(tokenKey: String) {
        sharedPreference.edit().putString(TOKEN_KEY, tokenKey).apply()
    }

    private fun storeExpiredDateToken(expiredAt: String) {
        sharedPreference.edit().putString(TOKEN_EXPIRED_AT, expiredAt).apply()
    }
    // endregion

    companion object {
        private const val TOKEN_KEY = "token_key"
        private const val TOKEN_EXPIRED_AT = "token_expired_at"
    }
}