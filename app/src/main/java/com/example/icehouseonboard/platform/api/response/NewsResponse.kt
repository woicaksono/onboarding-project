package com.example.icehouseonboard.platform.api.response

import com.example.icehouseonboard.domain.model.News
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DataNewsResponse(val data: List<NewsResponse>)

@Serializable
data class NewsResponse(
    val id: Int,
    val title: String,
    val url: String,
    val channel: ChannelResponse,
    val counter: CounterResponse,
    @SerialName("nsfw") val isNsfw: Boolean,
    @SerialName("cover_image") val coverImage: String,
    @SerialName("created_at") val createdAt: String,
) {
    companion object {
        fun NewsResponse.mapToDomainModel(): News {
            return News(
                id = id,
                title = title,
                url = url,
                coverImage = coverImage,
                isNsfw = isNsfw,
                channelName = channel.name,
                upVote = counter.upvote,
                downVote = counter.downvote,
                commentsCount = counter.comment,
                viewCount = counter.view
            )
        }
    }
}

@Serializable
data class ChannelResponse(val id: Int, val name: String)

@Serializable
data class CounterResponse(val upvote: Int, val downvote: Int, val comment: Int, val view: Int)