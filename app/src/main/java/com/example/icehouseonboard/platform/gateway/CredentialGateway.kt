package com.example.icehouseonboard.platform.gateway

import com.example.icehouseonboard.domain.model.Token
import com.example.icehouseonboard.platform.api.CredentialApi
import com.example.icehouseonboard.platform.api.request.LoginRequest
import com.example.icehouseonboard.platform.api.response.AuthResponse
import com.example.icehouseonboard.utils.DateFormatter
import javax.inject.Inject

interface CredentialGateway {
    suspend fun login(username: String, passwd: String): Result<Token>
    suspend fun refreshToken(): Result<Token>
}

class CredentialGatewayImpl @Inject constructor(
    val api: CredentialApi,
) : CredentialGateway {

    override suspend fun login(username: String, passwd: String): Result<Token> {
        return safelyExecute {
            api.login(LoginRequest(username, passwd)).let(::mapAsToken)
        }
    }

    override suspend fun refreshToken(): Result<Token> {
        return safelyExecute {
            api.requestToken().let(::mapAsToken)
        }
    }

    private fun mapAsToken(response: AuthResponse) =
        Token(
            response.token,
            response.scheme,
            DateFormatter().stringToDate(response.expiredAt)
        )
}
