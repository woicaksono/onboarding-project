package com.example.icehouseonboard.platform.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.icehouseonboard.platform.database.dao.NewsDao
import com.example.icehouseonboard.platform.database.dao.ProfileDao
import com.example.icehouseonboard.platform.database.entities.News
import com.example.icehouseonboard.platform.database.entities.Profile

@Database(
    entities = [News::class, Profile::class],
    version = 1,
    exportSchema = true,
)
abstract class OnBoardDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
    abstract fun profileDao(): ProfileDao
}
