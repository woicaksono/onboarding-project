package com.example.icehouseonboard.platform.gateway

suspend fun <Type> safelyExecute(job: suspend () -> Type): Result<Type> {
    return try {
        Result.success(job())
    } catch (error: Exception) {
        Result.failure(error)
    }
}
