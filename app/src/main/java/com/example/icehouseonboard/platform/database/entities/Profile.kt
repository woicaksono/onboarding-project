package com.example.icehouseonboard.platform.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.icehouseonboard.domain.model.Profile as ProfileDomain

@Entity
data class Profile(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "profile_id")
    val id: Int,
    @ColumnInfo(name = "user_name")
    val username: String,
    val name: String,
    val bio: String,
    val web: String,
    @ColumnInfo(name = "picture_path")
    val picturePath: String
)

fun ProfileDomain.convertToEntity(): Profile {
    return Profile(
        id = 0,
        username = userName,
        name = name,
        bio = bio,
        web = web,
        picturePath = picture,
    )
}
