package com.example.icehouseonboard.platform.api.request

import kotlinx.serialization.Serializable

@Serializable
data class LoginRequest(val username: String, val password: String)
