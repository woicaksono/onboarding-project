package com.example.icehouseonboard.platform.api

import com.example.icehouseonboard.platform.api.request.LoginRequest
import com.example.icehouseonboard.platform.api.response.AuthResponse
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface CredentialApi {

    @POST("/auth/login")
    suspend fun login(@Body loginRequest: LoginRequest): AuthResponse

    @Headers(REQUIRED_AUTH)
    @POST("/auth/token")
    suspend fun requestToken(): AuthResponse
}
