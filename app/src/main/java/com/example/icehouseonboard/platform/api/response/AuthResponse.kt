package com.example.icehouseonboard.platform.api.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonNames

@Serializable
data class AuthResponse(
    val token: String,
    val scheme: String,
    @SerialName("expires_at")
    val expiredAt: String,
)

@Serializable
data class AuthFailedResponse(
    val message: String,
    val fields: List<FieldErrorResponse>? = emptyList()
)

@Serializable
data class FieldErrorResponse(val name: String, val error: String)