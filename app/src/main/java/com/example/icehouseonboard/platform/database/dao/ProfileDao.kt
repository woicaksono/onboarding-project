package com.example.icehouseonboard.platform.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.icehouseonboard.platform.database.entities.Profile

@Dao
interface ProfileDao {
    @Query("SELECT profile_id, user_name, name, bio, web, picture_path FROM profile LIMIT 1")
    suspend fun getProfile(): Profile?

    @Insert
    suspend fun insertNewProfile(profile: Profile): Long

    @Query("DELETE FROM profile")
    suspend fun clearProfile(): Int
}
