package com.example.icehouseonboard.platform.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.icehouseonboard.domain.model.News as NewsDomain

@Entity(tableName = "news")
data class News(
    @PrimaryKey
    @ColumnInfo(name = "news_id")
    val id: Int,
    val title: String,
    val url: String,
    @ColumnInfo(name = "image_path")
    val imagePath: String,
    @ColumnInfo(name = "is_nsfw")
    val isNsfw: Boolean,
    @ColumnInfo(name = "channel_name")
    val channelName: String,
    @ColumnInfo(name = "up_vote")
    val upVote: Int,
    @ColumnInfo(name = "down_vote")
    val downVote: Int,
    @ColumnInfo(name = "comment_count")
    val commentsCount: Int,
    @ColumnInfo(name = "view_count")
    val viewCount: Int,
)

fun NewsDomain.convertAsEntity(): News {
    return News(
        id = id,
        title = title,
        url = url,
        imagePath = coverImage,
        isNsfw = isNsfw,
        channelName = channelName,
        upVote = upVote,
        downVote = downVote,
        commentsCount = commentsCount,
        viewCount = viewCount
    )
}
