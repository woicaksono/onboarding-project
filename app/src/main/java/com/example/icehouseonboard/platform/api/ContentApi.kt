package com.example.icehouseonboard.platform.api

import com.example.icehouseonboard.platform.api.response.DataNewsResponse
import com.example.icehouseonboard.platform.api.response.ProfileResponse
import retrofit2.http.GET
import retrofit2.http.Headers

interface ContentApi {

    @Headers(REQUIRED_AUTH)
    @GET("/me/profile")
    suspend fun getProfile(): ProfileResponse

    @Headers(REQUIRED_AUTH)
    @GET("/me/news")
    suspend fun getNews(): DataNewsResponse
}
