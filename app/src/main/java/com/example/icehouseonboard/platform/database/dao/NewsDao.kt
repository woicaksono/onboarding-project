package com.example.icehouseonboard.platform.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import androidx.room.Transaction
import com.example.icehouseonboard.platform.database.entities.News

@Dao
interface NewsDao {

    @Transaction
    @Query("SELECT * FROM News")
    suspend fun getNewsList(): List<News>

    @Insert(onConflict = IGNORE)
    suspend fun insertAllNews(news: List<News>)
}
