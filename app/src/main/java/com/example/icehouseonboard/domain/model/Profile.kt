package com.example.icehouseonboard.domain.model

data class Profile(
    val userName: String,
    val name: String,
    val bio: String,
    val web: String,
    val picture: String
)
