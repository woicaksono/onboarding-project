package com.example.icehouseonboard.domain.model

import okio.IOException
import java.util.*

data class AuthFailureException(val reason: FailedReason) : IOException()
data class Token(val key: String, val scheme: String, val expiresAt: Date)

//will keep this, to store <Key, Pair> error message from the server later.
data class FailedReason(val message: String)
