package com.example.icehouseonboard.domain.model

data class News(
    val id: Int,
    val title: String,
    val url: String,
    val coverImage: String,
    val isNsfw: Boolean,
    val channelName: String,
    val upVote: Int,
    val downVote: Int,
    val commentsCount: Int,
    val viewCount: Int,
)
