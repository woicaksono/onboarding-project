package com.example.icehouseonboard.domain.usecase

import com.example.icehouseonboard.domain.model.Token
import com.example.icehouseonboard.domain.usecase.RefreshTokenUseCaseImpl.None
import com.example.icehouseonboard.platform.gateway.CredentialGateway
import com.example.icehouseonboard.platform.preference.CachedTokenManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RefreshTokenUseCaseImpl @Inject constructor(
    private val gateway: CredentialGateway,
    private val cachedTokenManager: CachedTokenManager
) : BaseUseCase<Token, None>() {

    override suspend fun run(params: None): Result<Token> {
        return gateway.refreshToken()
            .onSuccess(cachedTokenManager::cacheToken)
    }

    object None
}
