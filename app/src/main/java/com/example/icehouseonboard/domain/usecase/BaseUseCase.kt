package com.example.icehouseonboard.domain.usecase

import kotlinx.coroutines.*

abstract class BaseUseCase<out Type, in Params>(
    private val mainDispatcher: CoroutineDispatcher = Dispatchers.Main,
    private val workDispatcher: CoroutineDispatcher = Dispatchers.IO
) where Type : Any {

    abstract suspend fun run(params: Params): Result<Type>

    operator fun invoke(
        params: Params,
        scope: CoroutineScope,
        onResult: (Result<Type>) -> Unit = {}
    ) {
        scope.launch(mainDispatcher) {
            val deferredResult = async(workDispatcher) {
                run(params)
            }

            onResult(deferredResult.await())
        }
    }

    object None
}
