package com.example.icehouseonboard.domain.usecase

import com.example.icehouseonboard.domain.model.Token
import com.example.icehouseonboard.domain.usecase.LoginUseCase.Params
import com.example.icehouseonboard.platform.gateway.CredentialGateway
import com.example.icehouseonboard.platform.preference.CachedTokenManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoginUseCase @Inject constructor(
    private val credentialGateway: CredentialGateway,
    private val cachedTokenManager: CachedTokenManager,
) : BaseUseCase<Token, Params>() {

    override suspend fun run(params: Params): Result<Token> {
        return credentialGateway.login(params.username, params.password)
            .onSuccess(cachedTokenManager::cacheToken)
    }

    data class Params(val username: String, val password: String)
}
