package com.example.icehouseonboard.platform.database

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.icehouseonboard.platform.database.entities.Profile
import com.example.icehouseonboard.sharedTest.OnBoardDatabaseRule
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class ProfileDaoTest {

    @get:Rule
    val rule = OnBoardDatabaseRule()

    @Test
    fun testWriteAndRead() {
        val profileDao = rule.dbAccess.profileDao

        rule.testScope.runBlockingTest {
            val profile = newProfileEntity(
                withUserName = "ucok99",
                withName = "Ucok",
            )
            profileDao.insertNewProfile(profile)

            val profileFromDb = profileDao.getProfile()

            assertTrue(profileFromDb?.id == 1)
            assertTrue(profileFromDb?.username == "ucok99")
            assertTrue(profileFromDb?.name == "Ucok")
        }
    }

    @Test
    fun testDelete() {
        val profileDao = rule.dbAccess.profileDao

        rule.testScope.runBlockingTest {
            val profile = newProfileEntity(
                withUserName = "ucok99",
                withName = "Ucok",
            )
            profileDao.insertNewProfile(profile)

            profileDao.clearProfile()

            val profileFromDb = profileDao.getProfile()

            assertTrue(profileFromDb == null)
        }
    }


    companion object {
        private fun newProfileEntity(
            withUserName: String = "",
            withName: String = "",
            withBio: String = "",
            withWeb: String = "",
            withPicture: String = "onboard.picture"
        ): Profile {
            return Profile(
                id = 0,
                username = withUserName,
                name = withName,
                bio = withBio,
                web = withWeb,
                picturePath = withPicture
            )
        }
    }
}
