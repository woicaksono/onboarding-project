package com.example.icehouseonboard.platform.database

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.icehouseonboard.platform.database.entities.News
import com.example.icehouseonboard.sharedTest.OnBoardDatabaseRule
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class NewsDaoTest {

    @get:Rule
    val rule = OnBoardDatabaseRule()

    @Test
    fun testWriteAndRead() {
        val newsDao = rule.dbAccess.newsDao

        rule.testScope.runBlockingTest {
            val newsList = listOf(
                newNewsEntity(withId = 1, withTitle = "Berita 1"),
                newNewsEntity(withId = 2, withTitle = "Berita 2")
            )

            newsDao.insertAllNews(newsList)

            val newsFromDb = newsDao.getNewsList()

            assertTrue(newsFromDb.size == 2)
        }
    }

    @Test
    fun testWriteMultipleId() {
        val newsDao = rule.dbAccess.newsDao

        rule.testScope.runBlockingTest {
            val newsList = listOf(
                newNewsEntity(withId = 1, withTitle = "Berita 1"),
                newNewsEntity(withId = 2, withTitle = "Berita 2")
            )

            newsDao.insertAllNews(newsList)
            newsDao.insertAllNews(newsList)
            newsDao.insertAllNews(newsList)
            newsDao.insertAllNews(newsList)

            val newsFromDb = newsDao.getNewsList()

            assertTrue(newsFromDb.size == 2)
        }
    }

    companion object {
        fun newNewsEntity(
            withId: Int = 1,
            withTitle: String = "",
            withUrl: String = "",
            withCoverImage: String = "",
            withIsNsfw: Boolean = false,
            withChannelName: String = "",
            withUpVote: Int = 0,
            withDownVote: Int = 0,
            withCommentsCount: Int = 0,
            withViewCount: Int = 0,
        ): News {
            return News(
                withId,
                withTitle,
                withUrl,
                withCoverImage,
                withIsNsfw,
                withChannelName,
                withUpVote,
                withDownVote,
                withCommentsCount,
                withViewCount,
            )
        }
    }
}