package com.example.icehouseonboard.sharedTest

import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.example.icehouseonboard.platform.database.DatabaseAccessor
import com.example.icehouseonboard.platform.database.DatabaseAccessorImpl
import com.example.icehouseonboard.platform.database.OnBoardDatabase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.asExecutor
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import org.junit.rules.TestWatcher
import org.junit.runner.Description

@ExperimentalCoroutinesApi
class OnBoardDatabaseRule : TestWatcher() {

    lateinit var dbAccess: DatabaseAccessor
    private val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    override fun starting(description: Description?) {
        super.starting(description)

        val db = initDb()

        dbAccess = DatabaseAccessorImpl(db)
    }

    override fun finished(description: Description?) {
        super.finished(description)
        dbAccess.close()
    }

    private fun initDb(): OnBoardDatabase {
        return Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            OnBoardDatabase::class.java
        ).setTransactionExecutor(testDispatcher.asExecutor())
            .setQueryExecutor(testDispatcher.asExecutor())
            .build()
    }
}