package com.example.icehouseonboard.sharedTest

import com.example.icehouseonboard.platform.preference.CachedTokenManager
import com.example.icehouseonboard.utils.NetworkInterceptor
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
class MockWebServerTestRule(
    val testDispatcher: TestCoroutineDispatcher = TestCoroutineDispatcher()
) : TestWatcher() {

    val cacheManager: CachedTokenManager = mock()

    private val server = MockWebServer().apply { url("/") }

    override fun starting(description: Description?) {
        super.starting(description)
        Dispatchers.setMain(testDispatcher)
    }

    override fun finished(description: Description?) {
        super.finished(description)
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    fun respondWithSuccess(withResponseBody: String) {
        respond(withResponseCode = 200, withResponseBody)
    }

    fun respondWithFailed(withResponseCode: Int, withResponseBody: String) {
        respond(withResponseCode, withResponseBody)
    }

    fun <Type> createApi(apiClazz: Class<Type>): Type {
        val networkInterceptor = NetworkInterceptor(cacheManager)
        val client = OkHttpClient.Builder().apply {
            addInterceptor(networkInterceptor.getInterceptor())
            callTimeout(500, TimeUnit.SECONDS)
        }.build()

        return Retrofit.Builder()
            .baseUrl(server.url("/").toString())
            .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))
            .client(client)
            .build()
            .create(apiClazz)
    }

    fun getRequest() = server.takeRequest()

    private fun respond(withResponseCode: Int, withResponseBody: String) {
        val mockResponse =
            MockResponse().setResponseCode(withResponseCode).setBody(withResponseBody)
        server.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return mockResponse
            }
        }
    }
}