package com.example.icehouseonboard.domain.usecase

import com.example.icehouseonboard.domain.model.AuthFailureException
import com.example.icehouseonboard.domain.model.FailedReason
import com.example.icehouseonboard.domain.model.Token
import com.example.icehouseonboard.domain.usecase.LoginUseCase.Params
import com.example.icehouseonboard.platform.gateway.CredentialGateway
import com.example.icehouseonboard.platform.preference.CachedTokenManager
import com.example.icehouseonboard.sharedTest.CoroutineTestRule
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@ExperimentalCoroutinesApi
class LoginUseCaseTest {

    private val gateway: CredentialGateway = mock()
    private val cachedTokenManager: CachedTokenManager = mock()
    private val useCase = LoginUseCase(gateway, cachedTokenManager)

    @get:Rule
    val rule = CoroutineTestRule()

    @Test
    fun `given response from gateway is success should pass as success and cache token`() {
        rule.testDispatcher.runBlockingTest {
            val date = Date()
            whenever(gateway.login("username", "password")).thenReturn(
                suspendCoroutine {
                    it.resume(
                        Result.success(
                            Token("aaaa", "Bearer", date)
                        )
                    )
                }
            )

            val result = useCase.run(Params("username", "password"))
            rule.testDispatcher.advanceUntilIdle()

            assertTrue(result.isSuccess)
            verify(cachedTokenManager).cacheToken(Token("aaaa", "Bearer", date))
        }
    }

    @Test
    fun `given response from gateway is failure should pass as Failure`() {
        rule.testDispatcher.runBlockingTest {
            val mockResult =
                Result.failure<Token>(AuthFailureException(FailedReason("Error From Server")))
            whenever(gateway.login("", "")).thenAnswer { mockResult }

            val result = useCase.run(Params("", ""))

            assertFalse(result.isSuccess)
            verifyNoMoreInteractions(cachedTokenManager)
        }
    }

    @After
    fun tearDown() {
        reset(gateway)
    }
}
