package com.example.icehouseonboard.platform.gateway

import com.example.icehouseonboard.domain.model.AuthFailureException
import com.example.icehouseonboard.domain.model.FailedReason
import com.example.icehouseonboard.domain.model.Token
import com.example.icehouseonboard.platform.api.CredentialApi
import com.example.icehouseonboard.sharedTest.MockWebServerTestRule
import com.example.icehouseonboard.utils.DateFormatter
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import java.util.*

@ExperimentalCoroutinesApi
class CredentialGatewayImplTest {

    @get:Rule
    val server = MockWebServerTestRule()

    private val api = server.createApi(CredentialApi::class.java)
    private val gateway: CredentialGateway = CredentialGatewayImpl(api)

    @Test
    fun `given response api is 200 when login api called should map as Success with token model`() {
        runBlocking {
            server.respondWithSuccess(withResponseBody = successAuthResponse)
            val expectedToken = newToken(
                withKey = "somerandomtoken",
                withScheme = "Bearer",
                withExpiredAt = DateFormatter().stringToDate("2019-08-24T14:15:22Z")
            )

            val result = gateway.login("username", "password")

            assertTrue(result.isSuccess)
            assertEquals(expectedToken, result.getOrNull())
        }
    }

    @Test
    fun `given response is 400 when login api called should map as Failed with AuthFailureException`() {
        runBlocking {
            server.respondWithFailed(withResponseCode = 400, withResponseBody = failedAuthResponse)
            val expectedResult =
                AuthFailureException(FailedReason("error-message-from-server"))

            val result = gateway.login("username", "password")

            assertTrue(result.isFailure)
            assertEquals(expectedResult, result.exceptionOrNull())
        }
    }

    @Test
    fun `given refreshToken requested should attach auth token in request header`() {
        runBlocking {
            val newToken = newToken(
                withKey = "somerandomtoken",
                withScheme = "Bearer",
                withExpiredAt = DateFormatter().stringToDate("2019-08-24T14:15:22Z")
            )
            val oldToken = "Scheme Token"
            whenever(server.cacheManager.getToken()).thenReturn(oldToken)
            server.respondWithSuccess(withResponseBody = successAuthResponse)

            val result = gateway.refreshToken()
            val header = server.getRequest().getHeader("Authorization")

            assertEquals("Scheme Token", header)
            assertEquals(newToken, result.getOrNull())
        }
    }

    companion object {
        private val successAuthResponse: String = """
            {
                "token": "somerandomtoken",
                "scheme": "Bearer",
                "expires_at": "2019-08-24T14:15:22Z"
            }
        """.trimIndent()

        private val failedAuthResponse = """
            {
                "message": "error-message-from-server"
            }
        """.trimIndent()

        fun newToken(
            withKey: String = "",
            withScheme: String = "",
            withExpiredAt: Date = Date()
        ): Token {
            return Token(key = withKey, scheme = withScheme, expiresAt = withExpiredAt)
        }
    }
}