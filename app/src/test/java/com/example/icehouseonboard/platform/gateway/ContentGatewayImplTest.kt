package com.example.icehouseonboard.platform.gateway

import com.example.icehouseonboard.domain.model.AuthFailureException
import com.example.icehouseonboard.domain.model.FailedReason
import com.example.icehouseonboard.domain.model.News
import com.example.icehouseonboard.domain.model.Profile
import com.example.icehouseonboard.platform.api.ContentApi
import com.example.icehouseonboard.sharedTest.MockWebServerTestRule
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class ContentGatewayImplTest {

    @get:Rule
    val server = MockWebServerTestRule()

    private val api = server.createApi(ContentApi::class.java)
    private val gateway = ContentGatewayImpl(api)

    @Test
    fun `given response is success when called getProfile should return success with Profile domain model`() {
        runBlocking {
            server.respondWithSuccess(withResponseBody = successProfileResponse)
            whenever(server.cacheManager.getToken()).thenReturn("token")

            val expectedProfile = newProfile(
                withUserName = "username",
                withName = "John Doe",
                withBio = "Lorem ipsum dolor si jamet",
                withWeb = "https://example.com",
                withPicture = "https://img.example.com/profile.jpg"
            )

            val result = gateway.getProfile()

            verifyHeaderAttached(withToken = "token")
            assertTrue(result.isSuccess)
            assertEquals(expectedProfile, result.getOrNull())
        }
    }

    @Test
    fun `given response is failed with 401 when called getProfile should return failed with AuthFailureException`() {
        runBlocking {
            server.respondWithFailed(
                withResponseCode = 401,
                withResponseBody = failedResponse
            )
            whenever(server.cacheManager.getToken()).thenReturn("token")

            val expectedResult = AuthFailureException(FailedReason("error-message-from-server"))

            val result = gateway.getProfile()

            verifyHeaderAttached(withToken = "token")
            assertTrue(result.isFailure)
            assertEquals(expectedResult, result.exceptionOrNull())
        }
    }

    @Test
    fun `given response is success when called getNews should return Success with News Domain`() {
        runBlocking {
            server.respondWithSuccess(withResponseBody = successNewsResponse)
            whenever(server.cacheManager.getToken()).thenReturn("token")

            val expectedNews = listOf(
                newNews(
                    withTitle = "Sample Title",
                    withUrl = "https://example.com",
                    withCoverImage = "https://place-hold.it/600x400/aaa/000000.jpg&text=SampleImage&bold&fontsize=20",
                    withIsNsfw = true,
                    withChannelName = "r/sample",
                    withUpVote = 2,
                    withDownVote = 3,
                    withCommentsCount = 4,
                    withViewCount = 5
                )
            )

            val result = gateway.getNews()

            verifyHeaderAttached(withToken = "token")
            assertTrue(result.isSuccess)
            assertEquals(expectedNews, result.getOrNull())
        }
    }

    @Test
    fun `given response is failed with 401 when called getNews should return failed with AuthFailureException`() {
        runBlocking {
            server.respondWithFailed(
                withResponseCode = 401,
                withResponseBody = failedResponse
            )
            whenever(server.cacheManager.getToken()).thenReturn("token")

            val expectedResult = AuthFailureException(FailedReason("error-message-from-server"))

            val result = gateway.getNews()

            verifyHeaderAttached(withToken = "token")
            assertTrue(result.isFailure)
            assertEquals(expectedResult, result.exceptionOrNull())
        }
    }

    private fun verifyHeaderAttached(withToken: String) {
        val header = server.getRequest().getHeader("Authorization")
        assertEquals("token", header)
    }

    companion object {
        private val successProfileResponse = """
            {
                "username": "username",
                "name": "John Doe",
                "bio": "Lorem ipsum dolor si jamet",
                "web": "https://example.com",
                "picture": "https://img.example.com/profile.jpg"
            }
        """.trimIndent()

        private val failedResponse = """
            {
                "message": "error-message-from-server"
            }
        """.trimIndent()

        const val successNewsResponse = """
            {
                "data": [
                    {
                        "id": 1,
                        "title": "Sample Title",
                        "url": "https://example.com",
                        "cover_image": "https://place-hold.it/600x400/aaa/000000.jpg&text=SampleImage&bold&fontsize=20",
                        "nsfw": true,
                        "channel": {
                            "id": 2,
                            "name": "r/sample"
                        },
                        "counter": {
                            "upvote": 2,
                            "downvote": 3,
                            "comment": 4,
                            "view": 5
                        },
                        "created_at": "2019-08-24T14:15:22Z"
                    }
                ]
            }
        """

        fun newProfile(
            withUserName: String = "",
            withName: String = "",
            withBio: String = "",
            withWeb: String = "",
            withPicture: String = "",
        ): Profile {
            return Profile(
                withUserName,
                withName,
                withBio,
                withWeb,
                withPicture,
            )
        }

        fun newNews(
            withId: Int = 1,
            withTitle: String = "",
            withUrl: String = "",
            withCoverImage: String = "",
            withIsNsfw: Boolean = false,
            withChannelName: String = "",
            withUpVote: Int = 0,
            withDownVote: Int = 0,
            withCommentsCount: Int = 0,
            withViewCount: Int = 0,
        ): News {
            return News(
                withId,
                withTitle,
                withUrl,
                withCoverImage,
                withIsNsfw,
                withChannelName,
                withUpVote,
                withDownVote,
                withCommentsCount,
                withViewCount,
            )
        }
    }
}